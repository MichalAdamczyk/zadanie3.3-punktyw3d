package pl.sda;

public class Punkt {

    private String name = null;

    public String getName() {
        return name;
    }


    int x;
    int y;
    int z;



    public Punkt(String name, int x, int y, int z) {

        this.name = name;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }
}
