package pl.sda;

import java.text.DecimalFormat;
import java.util.*;

public class Main {


    public static void main(String[] args) {
	/*

	Zadanie 3.3
3D
Zdefiniuj klasę Punkt służącą do przechowywania współrzędnych punktów w trójwymiarowej przestrzeni kartezjańskiej.
Napisz funkcję, która otrzymuje jako argumenty tablicę tab o argumentach typu Point oraz jej rozmiar,
i zwraca jako wartość najmniejszą spośród odległości pomiędzy punktami przechowywanymi w tablicy tab, razem z tymi punktami.
Zakładamy, że otrzymana w argumencie tablica ma co najmniej dwa argumenty.
	 */


        Scanner sc = new Scanner(System.in);

        Punkt punkt0 = new Punkt("punkt0", 87, 67, 10);
        Punkt punkt1 = new Punkt("punkt1", 23, 12, 9);
        Punkt punkt2 = new Punkt("punkt2", 3, 56, 90);
        Punkt punkt3 = new Punkt("punkt3", 7, 23, 45);

        Punkt[] tab = new Punkt[4];

        tab[0] = punkt0;
        tab[1] = punkt1;
        tab[2] = punkt2;
        tab[3] = punkt3;



        /*for (Punkt p :tab) {

            System.out.println(p.getName());

        }*/


        DecimalFormat df = new DecimalFormat("#.##");
        List<Double> results = new ArrayList<>();
        List<String> comment = new ArrayList<>();

        double length = 0;
        String punkty = null;

        for (int i = 0; i < tab.length - 1; i++) {


            for (int j = 1; j < tab.length; j++) {

                if (i != j) {

                    double distance = Math.sqrt(Math.pow(tab[i].x - tab[j].x, 2) + Math.pow(tab[i].y - tab[j].y, 2)
                            + Math.pow(tab[i].z - tab[j].z, 2));

                    if (length == 0) {

                        length = distance;

                    } else if (distance < length) {

                        length = distance;
                        punkty = tab[i].getName() + " i " + tab[j].getName();
                    }

                    results.add(distance);

                    System.out.println("Odległośc między " + tab[i].getName() + ", a " + tab[j].getName() + " wynosi: " + df.format(distance));
                    //comment.add("Odległośc między " + tab[i].getName() + ", a " + tab[j].getName() + " wynosi: " + df.format(distance));


                }
            }

        }

        System.out.println();
        Collections.sort(results);

//        for (String el : comment) {
//
//            System.out.println(el);
//        }


        System.out.println(df.format(results.get(0)));

        System.out.println();

        System.out.println("Najmniejsza odległość, równa: " + df.format(length) + " - występuje pomiędzy punktami: " + punkty);


    }
}
